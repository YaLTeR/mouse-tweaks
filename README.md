#Migration notice
This repository was migrated to [GitHub](https://github.com/YaLTeR/MouseTweaks). All further development and support will be done there.

#Mouse Tweaks
A mod that enhances the inventory management by adding various additional functions to the usual mouse buttons.

#How to build
`gradlew setupCIWorkspace build`

#Compatibility
Mouse Tweaks should work with everything based on GuiContainer. If your GUI isn't based on GuiContainer, or if you want to provide additional compatibility, take a look at `src/api/java/yalter/mousetweaks/api/`.
